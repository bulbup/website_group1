<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<?php
		require("header.php");
	?>
		<div id="image">
        	<img src="image/news_banner.jpg" />
        </div>
        <div id="content">
        	<div id="left">
            	<p><span style="color:#06F; font-size:20px; font-weight:bold;">Takashi Shimizu</span><br/>CEO</p>
                <img src="image/aboutus_leadership_images1.jpg" style="margin-bottom:10px;"/>
                <p>Manager of software development, systems integration and outsourcing services as well as extensive global off-shore services delivery leading more than 5,000 engineers. He demonstrated those expertise in large IT services deal making and delivery on double and triple digits million USD deals as General Manager, Vice President, etc. in IBM (Japan, China and US). After joining transcosmos inc., Shimizu led the new services planning and technology development capabilities expansion during more than 5 years as Corporate Senior Officer and Corporate Adviser.</p>
                <img src="image/aboutus_leadership_images1_1.jpg" width="640px" />
            </div>
            <div id="right">
            	<p style="border-bottom:1px solid #F00; font-size:20px; font-weight:bold; color:#F00;"><span  style="border-bottom:3px solid #F00;">LEADERSHIP</p>
                <ul>
                	<li>
                    	<img src="image/aboutus_leadership_images2_0.jpg" />
                        <p style="color:#03C; font-size:18px;">Vicente Segarra Larrazabal</p>
                        <p>Computer Science Engineer with global expertise in Software & IT Solutions and strong technological background (Business Intelligence, Web and eCommerce, Mobile) along with Project Management...</p>
                        <div style="clear:left"></div>
                    </li>
                    <li>
                    	<img src="image/Leadership_3.jpg" />
                        <p style="color:#03C; font-size:18px;">Vicente Segarra Larrazabal</p>
                        <p>David (Del) has been working across many disciplines in the IT industry for 30 years.  Originally from Australia, Del worked as a software engineer in the defence industry, before branching out.../p>
                        <div style="clear:left"></div>
                    </li>
                    <li>
                    	<img src="image/Tera_ (2).png" />
                        <p style="color:#03C; font-size:18px;">Vicente Segarra Larrazabal</p>
                        <p>After graduating as Bachelor of Network and Information Network System, Terauchi has built his career at transcosmos inc. in Tokyo...</p>
                        <div style="clear:left"></div>
                    </li>
                    <li>
                    	<img src="image/Leadership_3.jpg" />
                        <p style="color:#03C; font-size:18px;">Vicente Segarra Larrazabal</p>
                        <p>Computer Science Engineer with global expertise in Software & IT Solutions and strong technological background (Business Intelligence, Web and eCommerce, Mobile) along with Project Management...</p>
                        <div style="clear:left"></div>
                    </li>
                    <li>
                    	<img src="image/Leadership_6.jpg" />
                        <p style="color:#03C; font-size:18px;">Vicente Segarra Larrazabal</p>
                        <p>Through 11 years of experience in Finance & Accounting including 6 years as Chief Financial Officer in Foreign Invested...</p>
                        <div style="clear:left"></div>
                    </li>
                    <li>
                    	<img src="image/Leadership_3.jpg" />
                        <p style="color:#03C; font-size:18px;">Vicente Segarra Larrazabal</p>
                        <p>After Graduating with a Bachelor of Engineering Degree in Computer Science Engineering from University of Natural and Science (Vietnam), Tram Anh has being working for over 20 years in software...</p>
                        <div style="clear:left"></div>
                    </li>
                </ul>
            </div>
        </div>
    <?php
		require("footer.php");
	?>
</body>
</html>