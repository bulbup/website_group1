<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<?php
		require("header.php");
	?>
		<div id="header">
        </div>
        <div id="center"></div>
        <div id="center1">
        	<ul>
            	<li>
                	<div id="image1"><img src="image/icon_1.png" /></div>
                    <div id="intro">
                    	<p style="padding-top:70px; font-size:22px; font-weight:bold;">Dedicated Agile Resources</p>
                        <p>Leveraging on our talented pool of skilled engineers and our software outsourcing know-how and expertise, we provide dedicated development resources that can be utilized to develop high-value...</p>
                    </div>
                </li>
                <li>
                	<div id="image2"><img src="image/icon_1.png" /></div>
                    <div id="intro">
                    	<p style="padding-top:70px; font-size:22px; font-weight:bold;">Project Delivery</p>
                        <p>Thanks to the collaboration between our experienced management team and our high-skilled engineers, we are able to develop software applications ensuring on-time delivery and high quality outcomes... </p>
                    </div>
                </li>
                <li>
                	<div id="image3"><img src="image/icon_1.png" /></div>
                    <div id="intro">
                    	<p style="padding-top:70px; font-size:22px; font-weight:bold;">Package Solution</p>
                        <p>Following our strategy focus on innovation and high-added value products and solutions development, our team develops unique outstanding packages that can be utilized by customers to fulfil their...</p>
                    </div>
                </li>
            </ul>
        </div>
        <div id="center2">
        	<p>OUR SERVICES</p>
            <ul>
            	<li>
                	<img src="image/dm_icon_3.png" />
                    <p style="color:#03F; font-size:24px; font-weight:bold;">Website Development</p>
                    <p>Our strong web development team follows web development best practices ensuring the best performance and client-side/server-side approach to optimize business requirements of our clients...</p>
                </li>
                <li>
                	<img src="image/dm_icon_3.png" />
                    <p style="color:#03F; font-size:24px; font-weight:bold;">Smart Device (Mobile) Solution</p>
                    <p>Our Mobile team has an extensive experience developing mobile applications (M-Commerce, Games and Enterprise Mobility Solutions), being able to provide best in class mobile services and solutions...</p>
                </li>
                <li>
                	<img src="image/dm_icon_3.png" />
                    <p style="color:#03F; font-size:24px; font-weight:bold;">E-Commerce System Construction</p>
                    <p>We provide best in class E-Commerce solutions based on our extensive development track record by our professional staff specializing in E-Commerce systems, membership websites............</p>
                </li>
                <li>
                	<img src="image/dm_icon_3.png" />
                    <p style="color:#03F; font-size:24px; font-weight:bold;">Testing</p>
                    <p>We offer software testing and test automation services covering functional and non-functional requirements to ensure that our client’s applications are fully tested from back-end to front-end,...</p>
                </li>
            </ul>
        </div>
        <div style="clear:left"></div>
        <div id="footer">
        	<p>LEADERSHIP</p>
            <div id="footer1">
            	<div class="border">
                	<div id="footer_left">
                        <p style="color:#03F; font-size:18px;">Takashi Shimizu</p>
                        <p>Chief Executive Officer</p>
                            <p>
                                Manager of software development, systems integration and outsourcing services as well as extensive global off-shore services delivery leading more than 5,000 engineers. He demonstrated those expertise in large IT services deal making and delivery on double and triple digits million USD deals as...
                            </p>
                            <img src="image/aboutus_leadership_images1.jpg" />
					</div>
                </div>
                <div class="border">
                    <div id="footer_right">
                         <p style="color:#03F; font-size:18px;">Vicente Segarra Larrazabal</p>
                        <p>Chief Operation Officer</p>
                        <p>Computer Science Engineer with global expertise in Software &amp; IT Solutions and strong technological background (Business Intelligence, Web and eCommerce, Mobile) along with Project Management and Leadership skills. Working experience in several complex software projects for big...</p>
                        <img src="image/aboutus_leadership_images2_0.jpg" />
                    </div>
                 </div>
              </div>
         </div>
    <?php
		require("footer.php");
	?>
</body>
</html>