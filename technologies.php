<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<?php
		require("header.php");
	?>
        <div id="header12">
        <div class="box" align="center">
            
            <div class="row">
					<div class="col-md-2 " data-toggle="tooltip" data-placement="bottom" title="IOS">
						<img src="image/ios.png" alt="" width="70">
					</div>
                <div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="android">
						<img src="image/android.jpg" alt="" width="70">
					</div>
					<div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="IOS">
						<img src="image/java.jpg" alt="" width="70">
					</div>
                <div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="android">
						<img src="image/android.jpg" alt="" width="70">
					</div>
                <div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="IOS">
						<img src="image/ios.png" alt="" width="70">
					</div>
                <div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="android">
						<img src="image/android.jpg" alt="" width="70">
					</div>
                <div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="IOS">
						<img src="image/sql.jpg" alt="" width="70">
					</div>
                <div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="android">
						<img src="image/android.jpg" alt="" width="70">
					</div>
					<div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="IOS">
						<img src="image/ios.png" alt="" width="70">
					</div>
                <div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="android">
						<img src="image/android.jpg" alt="" width="70">
					</div>
                <div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="IOS">
						<img src="image/ios.png" alt="" width="70">
					</div>
                <div class="col-md-2" data-toggle="tooltip" data-placement="bottom" title="android">
						<img src="image/android.jpg" alt="" width="70">
					</div>
				</div>
            </div>
            
        </div>
        
        <div id="tittle" align="center"> 
        <h3> TECHNOLOGIES </h3>
            <br>
        </div>
        <div id="content" align="center">
                <table style="width:70%" align="center">
                    
                        <tr class="tbtittle">
                            <th>DATABASE</th>
                            <th>LANGUAGES</th>
                            <th>FRAMEWORKS</th>
                            <th>E-COMERCE</th>
                        </tr>
                    
                
                        <tr>
                            <td>     
          <ul id="navigation">
              <li>mySQL</li>
              <li> Oracle</li>
              <li> Mongo DB</li>
              <li>PostgreSQL</li>
              <li>Orient DB</li>
              <li>SQlite</li>
                                </ul>
                                
                            </td>
                            <!-- end database -->
                            
                            <td>
                             <ul id="navigation">
              <li>
             Java
              </li>
              <li>
              PHP
              </li>
              <li>
              C#
              </li>
              <li>
              Android
              </li>
              <li>
              Objective-C
              </li>
              <li>
              Html5/JavaScript
              </li>
              
                                </ul>
                                </td>
                            <!-- end languages -->
                            <td valign="top">
                             <ul id="navigation">
              <li>
             Spring
              </li>
              <li>
              Tapestry
              </li>
              <li>
              Play
              </li>
              <li>
              Laravel
              </li>
              <li>
              Symfony
              </li>
              
                                </ul>
                                </td>
                            <!-- end frameworks -->
                             <td valign="top">
                             <ul id="navigation" >
              <li>
              Magento
              </li>
              <li>
              Ec-cube
              </li>
              
                                </ul>
                                </td>
                            <!-- end e-comerce -->
                        </tr>
            </table>
            <br> <br> <br>
                      <table style="width:70%" align="center">
                  
                        <tr class="tbtittle">
                            <th>CMS</th>
                            <th>CRM</th>
                            <th>AUTOMATION</th>
                            <th>MANAGEMENT</th>
                        </tr>
                    
               
                        <tr>
                            <td valign="top">     
          <ul id="navigation">
              <li>
              Drupal
              </li>
              <li>
              HeartCore
              </li>
              
                                </ul>
                                
                            </td>
                            <!-- end database -->
                            
                            <td valign="top">
                             <ul id="navigation">
                                 <li>
                                Safesforce
                                 </li>
                                </ul>
                                </td>
                            <!-- end languages -->
                            <td>
                             <ul id="navigation">
              <li>
              Jenkins
              </li>
              <li>
              Maven
              </li>
              <li>
              Ant
              </li>
              <li>
             Grunt
              </li>
              
              
                                </ul>
                                </td>
                            <!-- end frameworks -->
                             <td valign="top">
                             <ul id="navigation">
              <li>
              Redmine
              </li>
              <li>
              Alminium
              </li>
              
              
                                </ul>
                                </td>
                            <!-- end e-comerce -->
                        </tr>
                     
                </table>
            </div>

         
    
                
        
               
        
    
         
    
           
         
          
          
       